var Model = {};

(function() {    

    this.get_page = function($scope, $http, currentPage, currentPageServer, direction){ 
      $http({
          url: $scope.route_path+".json", 
          method: "GET",
          params: { search: $scope.search, currentPage: currentPageServer, direction: direction}
        }).success(function(data) {

          if (direction == "next") {            
            console.log(data.objects)
            $scope.currentPage=$scope.currentPage+1
            $scope.currentPageServer=$scope.currentPageServer+1
            $scope.lengthPage = data.objects.length //items que hay en la pagina actual
            ctrl.updateItemsPerPage($scope, data.objects) // actualizar items de $scope.objects
            ctrl.updateBtnDelete($scope); // switch para mostrar button delete
            $scope.loadingNext = false;  // switch para mostrar spinner de carga
            ctrl.selectedAfterLoad($scope, $scope.pagesItemsSelected[$scope.currentPageServer]); //checked items por pagina
            if ($scope.object_data) setTimeout(function() { $scope.show_object($scope.object_data) }, 0.1); //add class current row selecionada para ver

          } else if(direction == "last") { 

            $scope.currentPage=$scope.currentPage-1
            $scope.currentPageServer=$scope.currentPageServer-1
            $scope.lengthPage = data.objects.length 
            ctrl.updateItemsPerPage($scope, data.objects)
            $scope.loadingLast = false;
            ctrl.updateBtnDelete($scope);
            console.log($scope.pagesItemsSelected)
            ctrl.selectedAfterLoad($scope, $scope.pagesItemsSelected[$scope.currentPageServer]);
            if ($scope.object_data) setTimeout(function() { $scope.show_object($scope.object_data) }, 0.1); //add class current row selecionada para ver

          } else if (direction == "refresh") {
            console.log(data.objects)
            ctrl.deleteItemsLastPage($scope, ctrl.createArrayItemsSelect($scope))
            ctrl.currentPageAfterRemove($scope)
            $scope.lengthPage = data.objects.length
            $scope.total_objects = data.total_rows
            ctrl.updateItemsPerPage($scope, data.objects)
            ctrl.updateItemSelected($scope)
            ctrl.selectedAfterLoad($scope, $scope.pagesItemsSelected[$scope.currentPageServer]); //checked items por pagina
            console.log($scope.pagesItemsSelected)
            $scope.loadingDelete = false
            if ($scope.pagesItemsSelected[$scope.currentPageServer].length == 0) $scope.btnDelete = false
            $scope.loadingRefresh = false
          }


          console.log('Successfully get_page');         
        }).error(function() {
          console.log('Error search'); 
        });
    }


    //metodo que elimina items del lado del servidor.
    this.destroy = function($scope, $http, output) {
       var selected = ctrl.createArrayItemsSelect($scope);

        $http({
          url: $scope.route_path+"/delete.json", 
          method: "POST",
          params: { ids: selected.toString() }
        }).success(function(data) {
          //console.log('Successfully destroy');
          if(data.total >= 0) {
            $scope.total_objects = $scope.total_objects-selected.length; //restar items eliminados de $scope.total_objects para que funcione el calculo de las paginas 
            Model.get_page($scope, $http, $scope.currentPage, $scope.currentPageServer, "refresh")
           
            ctrl.currentPageAfterRemove($scope) //reubicar pagina luego de eliminar

            
            $scope.alert = true;
            $scope.msnAlert = "los datos han sido eliminados exitosamente.";
            $scope.statusAlert="success";
          } else {
            $scope.loadingDelete = false
            console.log("no authorize")
          }
          output(true);
        }).error(function() {
          $scope.alert = true;
          $scope.msnAlert = "los datos no pudieron ser eliminados.";
          $scope.statusAlert="danger";
          console.error('Failed to destroy.');
          output(false);
        });
    }

}).call(Model);
