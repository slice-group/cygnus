angular.module('admin').factory('model', [
  '$http', function($http) {
    var model = {};



  model.destroy = function($scope) {
    Model.destroy($scope, $http, function($scopeoutput){});
  };

  model.next_page = function($scope, direction){
      $scope.loadingNext = true;
      Model.get_page($scope, $http, $scope.currentPage, $scope.currentPageServer+1, direction);
  }

  model.last_page = function($scope, direction) {
    $scope.loadingLast = true;
    Model.get_page($scope, $http, $scope.currentPage, $scope.currentPageServer-1, direction);
  }

  model.refresh_page = function($scope, direction) {
    $scope.loadingRefresh = true;   
    Model.get_page($scope, $http, $scope.currentPage, $scope.currentPageServer, direction);
  }; 

  return model;
  }
]);