# Agregar datos de configuración
InyxContactUsRails.setup do |config|
	config.mailer_to = "info@cygnuscon.net"
	config.mailer_from = "info@cygnuscon.net"
	config.name_web = "CYGNUS CONOCIMIENTO"
	# Activar o Desactivar la ruta especificada ("messages/new")
	config.messages_new = true
	#Route redirection after send
	config.redirection = "/contact_us"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LcldgMTAAAAAH43xOtxrdUvDev0QOP9M8zWSz0e"
	  config.private_key = "6LcldgMTAAAAAOvbj4ccAl7KhsyG0s6VW4Se3nSC"
	end
end