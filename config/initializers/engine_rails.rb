require "#{Rails.root}/lib/engine_rails.rb"

EngineRails.setup do |config|
	
  config.routes_sitebar={}

  Rails::Engine.subclasses.each do |engine| 
  	if engine.parent.constants.include? :SITEBAR
  		config.routes_sitebar.merge!(engine.parent::SITEBAR)
  		config.routes_dropdown = nil
  	elsif engine.parent.constants.include? :DROPDOWN
  		config.routes_dropdown = engine.parent::DROPDOWN
  		config.routes_sitebar = nil
  	end
  end

  config.list_engines =  Rails::Engine::subclasses.map { |x| x.parent.to_s.underscore.to_sym }
  
end