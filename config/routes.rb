Rails.application.routes.draw do

  root to: 'frontend#home'
  get 'about', to: "frontend#about", as: "about"
  get 'services', to: "frontend#services", as: "services"
  get 'team', to: "frontend#team", as: "team"
  get 'clients', to: "frontend#clients", as: "clients"

  mount InyxContactUsRails::Engine, :at => '', as: 'messages'
  
  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
